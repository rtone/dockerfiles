# README #

Dockerfiles pour les images de base utilisées chez Rtone. Généralement basés sur Debian.

* Java8 : JDK 8 d'Oracle
* Asciidoctor : inspirée de celle référencée dans le projet [Asciidoctor](https://registry.hub.docker.com/u/gscheibel/asciidoctor/), mais basée sur Debian
